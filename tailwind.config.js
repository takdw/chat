const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  purge: ["./src/**/*.html", "./src/**/*.vue"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ["Inter", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        sent: "#dcf4fe",
        recieved: "#f2f6f9",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
